const log4js = require('log4js');

const Config = require('./src/Config');
const httpServer = require('./src/HttpServer');
const register = require('./src/Register');
const flagger = require('./src/Flagger');
const clientClass = require('./src/UpholdHttpClient');
const config = Config.getMergedConfig('./config/config.yaml');

// Logging
log4js.configure(config.logging);
const Flagger = new flagger(log4js, config.db)
const Register = new register(log4js, config.register, clientClass, Flagger)
const HttpServer = new httpServer(log4js, config.httpServer, Register)

// starts server, reg routes.
HttpServer.start()
