
const flagger = require('../../src/Flagger');


test("Constructor test, ensure that properties are set correctly", () => {
    const logger = {
        getLogger() { return {}}
    }
    const config = {
        host: "1",
        user: "1",
        pass: "1",
        database: "1",
    }

    jest.spyOn(flagger.prototype, 'prepDb').mockReturnValue({})
    let Flagger = new flagger(logger, config);
    expect(Flagger.logger).toEqual({});
    expect(Flagger.prepDb).toHaveBeenCalledWith({"client": "pg", "connection": {"database": "1", "host": "1", "password": "1", "user": "1"}});
});
