"use strict";

module.exports = class Register {

    constructor(logger, config, httpClientClass, flagger) {
        this.logger = logger.getLogger('Register');
        this.loggerInstance = logger;
        this.config = config;
        this.HttpClientClass = httpClientClass;
        this.flagger = flagger;
        this.registeredPairs = {};
    }

    registerPair({base, quote, interval, percentageChangeAlert = this.config.defaultPercentageChangeAlert}) {
        const pairName = Register.normalise(base + "-" + quote);
        this.logger.info('Registering pair %s with interval %s ms', pairName, interval);
        this.logger.info('Price change alert set at ', percentageChangeAlert);
        // We will use the pair name to store the existing client
        return new Promise((resolve, reject) => {
            if (!this.registeredPairs[pairName]) {
                this.registeredPairs[pairName] = new this.HttpClientClass(pairName, interval, percentageChangeAlert, this.loggerInstance, this.flagger);
                resolve(this.registeredPairs[pairName].getPairName());
            } else {
                const msg = "The pair already exists " + pairName;
                this.logger.error(msg);
                reject(msg);
            }
        });


    }

    static normalise(word) {
        return word.toUpperCase();
    }

};
