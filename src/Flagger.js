"use strict";

const knex = require('knex');
// Flagger will save the stuff to DB
module.exports = class Flagger {

    constructor(logger, config) {
        this.logger = logger.getLogger('Flagger');
        this.prepDb({
            client: 'pg',
            connection: {
                host: config.host,
                user: config.user,
                password: config.pass,
                database: config.database
            }
        });
    }

    prepDb(config) {
        this.knex = knex(config);
        /*
         * Ignored "Use async .hasTable to check if table exists and then use plain .createTable" its a POC
         */
        this.knex.schema.createTableIfNotExists("flags", (table) => {
            table.increments();

            table.string('datetime');
            table.string('pairName');
            table.string('startObject');
            table.string('flaggedExchange');
        });

    }

    insertFlag(pair, startObj, flaggedObj) {
        this.logger.info('Saving flag %s', pair);
        return this.knex("flags").insert([
            {
                datetime: new Date().toISOString(),
                pairName: pair,
                startObject: startObj,
                flaggedExchange: flaggedObj,
            },
        ]);
    }


};
