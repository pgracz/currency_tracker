"use strict";

const superagent = require('superagent');

module.exports = class UpholdHttpClient {

    // Will require inspector to be checking when to flag the price change
    constructor(pair, interval, percentageChangeAlert, logger, flagger) {
        this.pair = pair;
        this.interval = interval;
        this.percentageChangeAlert = percentageChangeAlert;
        this.logger = logger.getLogger('Log Pair ' + pair);
        this.baseUrl = 'https://api.uphold.com/v0/ticker/';
        // Bind for the access to class properties
        this.startTicking.bind(this);
        this.startTicking(interval);
        this.initialRecordStore = [];
        // Extend record store to use db
        this.flagger = flagger;
    }

    startTicking(interval) {
        this.getData(this.baseUrl + this.getPairName())
            .then((result) => {
                this.logger.info("startTicking resulted in : %o", result.body);

                this.initialRecordStore[0] = result.body;
                /*
                 * Schedule next tick if all went fine
                 * According to https://stackoverflow.com/questions/2130241/pass-correct-this-context-to-settimeout-callback
                 */
                setTimeout(() => {
                    this.tick();
                }, interval);
                return result.body;
            })
            .catch((err) => {
                console.log(err.message);
                this.logger.error('Initial tick failed, rescheduling for the next tick');
                this.logger.error(err.message);
                // If the first tick fails, schedule initial tick instead of the next tick
                setTimeout(() => {
                    this.startTicking();
                }, interval);
            });
    }

    tick() {
        this.getData(this.baseUrl + this.getPairName()).then((result) => {
            this.logger.info("Tick resulted in : %o", result.body);
            // Schedule next tick if all went fine
            setTimeout(() => {
                this.tick();
            }, this.interval);
            // Check if the new data needs to be reported for change of percentage
            if (this.comparePriceChange(result.body)) {
                // Assumption was that if their fluctuated to then we just gonna flag it
                this.flagger.insertFlag(this.getPairName(), this.initialRecordStore[0], result.body);
            }
        }).catch((err) => {
            this.logger.error('Tick failed, rescheduling for the next tick');
            this.logger.error(err.message);
            // If the tick fails, schedule initial tick instead of the next tick
            setTimeout(() => {
                this.startTicking();
            }, this.interval);
        });
    }

    getPairName() {
        return this.pair;
    }

    getData(url) {
        return superagent.get(url)
            .send();
    }


    // I never got to clarify the spread, so i guess we gonna compare both ask and and bid prices?
    comparePriceChange(resultObject) {
        let saveToFlagger = 0;
        if (this._getPercentageChange(resultObject.ask, this.initialRecordStore[0].ask) > this.percentageChangeAlert) {
            this.logger.error("Ask the price change was greater than the set alert!");

            saveToFlagger = 1;
        }

        if (this._getPercentageChange(resultObject.bid, this.initialRecordStore[0].bid) > this.percentageChangeAlert) {
            this.logger.error("Bid the price change was greater than the set alert!");
            saveToFlagger = 1;
        }
        return saveToFlagger;

    }

    // I am not sure about the description in the task: we want to bbe alerted if the price changed 0.01 percent, so 1%? ot 1 per-mille (‰)?
    _getPercentageChange(newPrice, startPrice) {
        // Math abs because we dont care which way the change swings. We multiply x10k because JS sucks with floats
        const k10 = 10000;
        const hundred = 100;
        return Math.abs((newPrice * k10 - startPrice * k10) / (startPrice * k10)) * hundred;
    }
};
