"use strict";

const express = require('express');
const bodyParser = require('body-parser');

module.exports = class HttpServer {

    constructor(logger, config, register) {
        this.logger = logger.getLogger('HttpServer');
        this.config = config;
        this.register = register;
        this.app = express();
        this.statuses = {
            badRequest: 400,
            ok: 200,
            internalErr: 500
        };
    }

    start() {
        // If you ever need middleware, this would be neat here. register routes and middleware.
        this.registerRoutes.bind(this);
        this.registerRoutes();

        this.app.listen(this.config.port, '0.0.0.0');
        this.logger.info("App listening on port %s", this.config.port);
    }

    registerRoutes() {
        /*
         * I guess here would bbe nice a piece of validation to check if the currency exists.
         * yes, we will use json
         */
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: false}));

        this.app.post('/track', (req, res) => {
            this.logger.debug('req.body: %o', req.body);
            if (req.body.base.match(this.config.currencyRegex) === null) {
                // Bad request bro.
                res.sendStatus(this.statuses.badRequest);
                this.logger.error('Failed validation!');
                return;
            }
            if (req.body.quote.match(this.config.currencyRegex) === null) {
                res.sendStatus(this.statuses.badRequest);
                this.logger.error('Failed validation!');
                return;
            }
            if (req.body.interval) {
                req.body.interval = req.body.interval.toString();
                if (req.body.interval.match(this.config.intervalRegex) === null) {
                    res.sendStatus(this.statuses.badRequest);
                    this.logger.error('Failed validation! Wrong interval format!');
                    return;
                }
            } else {
                // Default interval is 5000 ms, 5 sec
                req.body.interval = this.config.defaultInterval;
            }

            this.register.registerPair(req.body).then(() => {
                this.logger.debug('Base %s, quote: %s', req.body.base, req.body.quote);
                this.logger.debug('Registered interval: %s milliseconds', req.body.interval);
                res.status(this.statuses.ok).send({"registered": req.body});
            }).catch((err) => {
                res.status(this.statuses.internalErr).send({"Something went wrong": err});
            });

        });
    }

};
