FROM node:14-alpine as preinstall
LABEL name="sample - Prebuild"
WORKDIR /app
ADD  package.json package-lock.json  /app/
# use npm ci --production for production env
RUN npm ci --production

FROM node:14-alpine as copyfiles
LABEL name="sample - Copyfiles"
WORKDIR /app
ADD . /app
RUN rm -rf /app/node_modules

FROM node:14-alpine
LABEL name="sample ms for the task"
LABEL maintainer="Peter Gracz <gracz.peter@gmail.com>"
EXPOSE 80

WORKDIR /app
CMD [ "npm", "start" ]

COPY --from=copyfiles /app /app
COPY --from=preinstall /app/node_modules /app/node_modules
