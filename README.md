# currency_tracker

Finally, a task that seems to be worth doing!

to run in dev mode: docker-compose -f ./docker/dev.yaml up --build (build is not necessary if you already installed dependencies locally its dev box, so mounts stuff in and run nodemon)

to run proper build docker-compose -f ./docker/ci.yaml up --build

(for tests, details check package.json)

Http server by default server on port 3000. to Override, set env var to port you want in docker-compose (also update the port mapping!)

log level by default is set to warn, but in docker-compose changed to debug via env var for ease in live development.

only allowed currencies are the 3 letter ones (like EUR, eur, USD, usd, GBP, gbp etc). This is not including the more sophisticated currencies (like arabic ones). but hey its a POC.
When the currency doesnt match regex -> http code 400 (bad request)
Same for the interval.


The POST  endpoint: http://localhost:3000/track
params (json): 
```
{
    "base": "USD",
    "quote": "GBP",
    "interval": 2000
}
```
remember to set header content-type to application/json.
Currently supported json params: base (3 char), quote (3char), interval (ms)
it would be relatively easy to extend existing post request to be able to override any config built-ins.
The above would be easily extendable for oscillation or any other param really. up to you.
The endpoint will handle multiple currency pairs no problem, but each one only once. If you attempt to re-add the same pair it wont let you.
The records are stored to postres usinig knex, but there are no read endpoints (wasn't part of the scope of POC)
